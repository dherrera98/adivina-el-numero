﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsFormsApp2
{
    class IniciarJuego
    {
        int contador;
        Random rnd = new Random();
        int numeroSecreto;

        public IniciarJuego()
        {
            contador = 0;
            numeroSecreto = rnd.Next(1, 100);
        }

        public String comprobarNumeroRandom(int numeroUsuario)
        {
            if (numeroUsuario == numeroSecreto)
            {
                return "igual";
            }
            else if (numeroUsuario < numeroSecreto)
            {
                contador += 1;
                return "menor";
            }
            else
            {
                contador += 1;
                return "mayor";
            }
        }

        public int mostrarContador() => contador;
    }
}
