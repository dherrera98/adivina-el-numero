﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class juego : Form
    {
            IniciarJuego i = new IniciarJuego();

        public juego()
        {
            InitializeComponent();
            button1.Show();
            button2.Hide();
        }

        private void juego_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            comprobarNumero();
            actualizarContador();
            textBox1.Text = "";
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == '\r')
            {
                comprobarNumero();
                actualizarContador();
                textBox1.Text = "";
            }
        }

        private void comprobarNumero()
        {
            try
            {
                int numeroUsuario = int.Parse(textBox1.Text);
                label2.Show();
                String resultado = i.comprobarNumeroRandom(numeroUsuario);
                if (resultado == "igual")
                {
                    button1.Hide();
                    button2.Show();
                    label2.Text = "Has ganado en " + i.mostrarContador() + " turnos!!!!!!";
                    label2.ForeColor = Color.Green;
                    textBox1.Enabled = false;
                }
                else if (resultado == "menor"){
                    label2.Text = "Fallastes \r El número introducido es menor al secreto";
                    label2.ForeColor = Color.Red;
                }
                else
                {
                    label2.Text = "Fallastes \r El número introducido es mayor al secreto";
                    label2.ForeColor = Color.Red;
                }
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Error de formato");
            }
        }

        private void actualizarContador()
        {
            label3.Visible = true;
            label3.Text = "Intento: "+i.mostrarContador();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            i = new IniciarJuego();
            button2.Hide();
            button1.Show();
            textBox1.Enabled = true;
        }
    }
}
